<?php
#Name:Brute Force Ritchey Image Data v1
#Description:Do a brute force attack on an SHA-256 checksum of data conformed to Ritchey Image Data v1 by starting from rgb(0,0,0) and using content generation techniques to do valid increments until data with matching checksum is found. Returns data as a string success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Image data must contain a field called "Colour", and it must have an RGB colour code as the value. No other fields can be included in the data. The width, and height must be as equal as possible. Other resolutions are not supported.
#Arguments:'checksum' (required) is a string containing the SHA-256 checksum of Ritchey Image Data. 'match_number' (optional) is the number of the match to accept (eg: 1 means the first match, 2 means the second match), 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):checksum:string:required,match_number:number:optional,display_errors:bool:optional
#Content:
if (function_exists('brute_force_ritchey_image_data_v1') === FALSE){
function brute_force_ritchey_image_data_v1($checksum, $match_number = NULL, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@isset($checksum) === FALSE){
		$errors[] = "checksum";
	}
	if ($match_number === NULL){
		$match_number = 1;
	} else if (@is_numeric($match_number) === FALSE){
		$errors[] = "match_number";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Using a loop increment ritchey_image_data until matching SHA-256 checksum is found, and then decrement match_number. Repeat until match number is 0, and then return ritchey_image_data.]
	if (@empty($errors) === TRUE){
		###Set starting data
		$ritchey_image_data = '[Colour:0,0,0.]';
		$current_checksum = @hash('sha256', $ritchey_image_data);
		###using a loop increment ritchey_image_data until matching SHA-256 checksum is found, and then decrement match_number. Repeat until match number is 0, and then return ritchey_image_data.
		while ($match_number > 0){
			$match_number--;
			while ($checksum != $current_checksum){
				$location = realpath(dirname(__FILE__));
				require_once $location . '/dependencies/increment_ritchey_image_data_v1/increment_ritchey_image_data_v1.php';
				$ritchey_image_data = increment_ritchey_image_data_v1($ritchey_image_data, FALSE);
				$current_checksum = @hash('sha256', $ritchey_image_data);
			}
		}
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('brute_force_ritchey_image_data_v1_format_error') === FALSE){
				function brute_force_ritchey_image_data_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("brute_force_ritchey_image_data_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $ritchey_image_data;
	} else {
		return FALSE;
	}
}
}
?>